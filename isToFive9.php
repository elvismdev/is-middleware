<?php

require __DIR__ . '/vendor/autoload.php';

use ISMiddleWareApp\InfusionToFive9;



$app = new InfusionToFive9();


if ($app->sendToFive9()->error)
	echo 'Error: ' . $app->sendToFive9()->errorCode . ': ' . $app->sendToFive9()->errorMessage;
else
	echo $app->sendToFive9()->response;

