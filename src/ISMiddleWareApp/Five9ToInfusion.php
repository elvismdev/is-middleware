<?php

namespace ISMiddleWareApp;

require_once(__DIR__ . '/../../vendor/issdk/Infusionsoft/infusionsoft.php');

use Symfony\Component\HttpFoundation\Request;


class Five9ToInfusion
{
    /**
     * @var Infusionsoft_Lead
     */
    private $opportunity;

    /**
     * @var Infusionsoft_Stage
     */
    private $stage;

    /**
     * @var Infusionsoft_Contact
     */
    private $contact;

    /**
     * @var Infusionsoft_ContactAction
     */
    private $notes;

    /**
     * @var Infusionsoft_ContactAction
     */
    private $addNote;

    /**
     * @var array
     */
    private $isUser = array();

    /**
     * @var bool
     */
    private $allSuccess = true;

    /**
     * @var array|int
     */
    private $queryData = array();

    /**
     * Construct
     */
    public function __construct()
    {

        // Gather and organize data from the parameters send in the url query string
        $queryStr = Request::createFromGlobals()->getQueryString();
        parse_str($queryStr, $queryData);

        $this->queryData = $queryData;


        // Load the Contact.
        $this->contact = new \Infusionsoft_Contact( $this->get('contactId') );


        // Load the Opportunity/Lead.
        $this->opportunity = new \Infusionsoft_Lead( $this->get('opportunityId') );


        // Search the opportunity stage info so we can use his ID and Name later.
        $searchStage = \Infusionsoft_DataService::query( new \Infusionsoft_Stage(), array( 'StageName' => $this->get('salesStage') ) );
        $this->stage = array_shift( $searchStage );


        // Search the opportunity stage info so we can use his ID and Name later.
        $this->notes = \Infusionsoft_DataService::query( new \Infusionsoft_ContactAction(), array( 'ContactId' => $this->get('contactId'), 'ObjectType' => 'Note' ) );

        // Load ContactAction object in case we have to add a new Note.
        $this->addNote = new \Infusionsoft_ContactAction();


        // Lookup sales person in InfusionSoft by his email.
        $isUserSearch = \Infusionsoft_DataService::query( new \Infusionsoft_User(), array( 'Email' => $this->get('isAgentEmail') ) );
        $this->isUser = array_shift( $isUserSearch );

    }


    public function get($index)
    {
        if (array_key_exists($index, $this->queryData))
            return $this->queryData[$index];

        return false;
    }

    /**
     * @return bool
     */
    public function isSuccess()
    {
        return ($this->allSuccess === true) ? "> Data successfully transferred from Five9 to InfusionSoft" : "> Something went wrong behind :(";
    }

    /**
     * Check if we have changes to do on the Contact information at InfusionSoft.
     *
     * @return bool
     */
    public function isContactChanged()
    {
        $changed = false;

        if ( ( $this->get('firstName') !== false ) && ( $this->contact->FirstName != $this->get('firstName') ) ) {
            $this->contact->FirstName = $this->get('firstName');
            $changed = true;
        }

        if ( ( $this->get('lastName') !== false ) && ( $this->contact->LastName != $this->get('lastName') ) ) {
            $this->contact->LastName = $this->get('lastName');
            $changed = true;
        }

        if ( ( $this->get('phone1') !== false ) && ( preg_replace( '/\D+/', '', $this->contact->Phone1 ) != $this->get('phone1') ) ) {
            $this->contact->Phone1 = $this->get('phone1');
            $changed = true;
        }

        if ( ( $this->get('phone2') !== false ) && ( preg_replace( '/\D+/', '', $this->contact->Phone2 ) != $this->get('phone2') ) ) {
            $this->contact->Phone2 = $this->get('phone2');
            $changed = true;
        }

        if ( ( $this->get('company') !== false ) && ( $this->contact->Company != $this->get('company') ) ) {
            $this->contact->Company = $this->get('company');
            $changed = true;
        }

        if ( ( $this->get('jobTitle') !== false ) && ( $this->contact->JobTitle != $this->get('jobTitle') ) ) {
            $this->contact->JobTitle = $this->get('jobTitle');
            $changed = true;
        }

        if ( ( $this->get('email') !== false ) && ( $this->contact->Email != $this->get('email') ) ) {
            $this->contact->Email = $this->get('email');
            $changed = true;
        }

        if ( ( $this->get('street') !== false ) && ( $this->contact->StreetAddress1 != $this->get('street') ) ) {
            $this->contact->StreetAddress1 = $this->get('street');
            $changed = true;
        }

        if ( ( $this->get('address2') !== false ) && ( $this->contact->StreetAddress2 != $this->get('address2') ) ) {
            $this->contact->StreetAddress2 = $this->get('address2');
            $changed = true;
        }

        if ( ( $this->get('city') !== false ) && ( $this->contact->City != $this->get('city') ) ) {
            $this->contact->City = $this->get('city');
            $changed = true;
        }

        if ( ( $this->get('state') !== false ) && ( $this->contact->State != $this->get('state') ) ) {
            $this->contact->State = $this->get('state');
            $changed = true;
        }

        if ( ( $this->get('zip') !== false ) && ( $this->contact->PostalCode != $this->get('zip') ) ) {
            $this->contact->PostalCode = $this->get('zip');
            $changed = true;
        }

        if ( ( $this->get('country') !== false ) && ( $this->contact->Country != $this->get('country') ) ) {
            $this->contact->Country = $this->get('country');
            $changed = true;
        }

        return $changed;
    }

    /**
     * @return bool
     */
    public function updateContact()
    {
        if ( $this->isContactChanged() === true ) {
            if ( !$this->contact->save() ) {
                $this->allSuccess = false;
            } else {
                return true;
            }
        }

        return false;
    }



    /**
     * Check if we have changes to make on the Opportunity at InfusionSoft.
     *
     * @return bool
     */
    public function isOpportunityChanged()
    {
        $changed = false;

        if ( ( $this->get('disposition') !== false ) && ( $this->opportunity->_Five9Disposition != $this->get('disposition') ) ) {
            $this->opportunity->_Five9Disposition = $this->get('disposition');
            $changed = true;
        }


        if ( ( $this->stage->Id ) && ( $this->opportunity->StageID != $this->stage->Id ) ) {
            $this->opportunity->StageID = $this->stage->Id;
            $changed = true;
        }

        return $changed;
    }

    /**
     * @return bool
     */
    public function updateOpportunity()
    {
        if ($this->isOpportunityChanged() === true) {
            if (!$this->opportunity->save()) {
                $this->allSuccess = false;
            } else {
                return true;
            }
        }

        return false;
    }



    /**
     * Check if we have a new note to add.
     *
     * @return bool
     */
    public function isNewNote()
    {

        $newNote = false;

        $noteSearchResults = array_filter(
            $this->notes,
            array($this, 'noteExist')
            );

        if ( !is_a( array_shift( $noteSearchResults ), 'Infusionsoft_ContactAction') ) {

           $this->addNote->ContactId = $this->contact->Id;
           $this->addNote->OpportunityId = $this->opportunity->Id;
           $this->addNote->UserID = $this->isUser->Id;
           $this->addNote->CreatedBy = $this->isUser->Id;
           $this->addNote->CompletionDate = date('Ymj\TG:i:s');
           $this->addNote->ActionDescription = 'Five9 Call Comment';
           $this->addNote->CreationNotes = $this->get('callComment');
           $this->addNote->ObjectType = 'Notes';
           $this->addNote->ActionType = 'Five9 Note';

           $newNote = true;

       }

       return $newNote;

   }

    /**
    * Look for a match.
    *
    * @return bool
    */
    private function noteExist($obj)
    {
        return ( $obj->ActionDescription == 'Five9 Call Comment' && $obj->CreationNotes == $this->get('callComment') );
    }



    /**
     * Add a note for contact and/or opportunity.
     */
    public function addNote()
    {

        if ( $this->isNewNote() === true ) {
            if ( !$this->addNote->save() ) {
                $this->allSuccess = false; // Return a false status in case save fails for any reason.
            } else {
                return true;
            }
        }

        return false;

    }

}